package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(reverse(123))
	fmt.Println(reverse(-120))
}


func reverse(x int) int {
	if x == 0 || x < math.MinInt32 || x > math.MaxInt32 {
        return 0
    }
	var reversedNum int = 0
    var remainder int
	//sign := 1
    //if x < 0 {
    //    sign = -1
    //}
    
	for x != 0 {
	remainder = x % 10
    reversedNum = reversedNum * 10 + remainder
    x /= 10
	}
	return reversedNum	
}